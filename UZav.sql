USE [Учебные заведения]
GO
/****** Object:  Table [dbo].[Города]    Script Date: 09/21/2015 14:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Города](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
 CONSTRAINT [PK_Города] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Города] ON
INSERT [dbo].[Города] ([id], [Название]) VALUES (1, N'Пермь                                                                                               ')
INSERT [dbo].[Города] ([id], [Название]) VALUES (2, N'Екатеринбург                                                                                        ')
INSERT [dbo].[Города] ([id], [Название]) VALUES (3, N'Новосибирск                                                                                         ')
INSERT [dbo].[Города] ([id], [Название]) VALUES (4, N'Москва                                                                                              ')
INSERT [dbo].[Города] ([id], [Название]) VALUES (5, N'Санкт-Петербург                                                                                     ')
SET IDENTITY_INSERT [dbo].[Города] OFF
/****** Object:  Table [dbo].[Университеты]    Script Date: 09/21/2015 14:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Университеты](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
	[id_Города] [int] NULL,
 CONSTRAINT [PK_Университеты] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Университеты] ON
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (1, N'ПГНИУ                                                                                               ', 1)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (2, N'ПНИПУ                                                                                               ', 1)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (3, N'ПГСХА                                                                                               ', 1)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (4, N'УрФУ                                                                                                ', 2)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (5, N'МГУ                                                                                                 ', 4)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (6, N'МФТИ                                                                                                ', 4)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (7, N'СпбГУ                                                                                               ', 5)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (8, N'ИТМО                                                                                                ', 5)
INSERT [dbo].[Университеты] ([id], [Название], [id_Города]) VALUES (9, N'НГУ                                                                                                 ', 3)
SET IDENTITY_INSERT [dbo].[Университеты] OFF
/****** Object:  Table [dbo].[Факультеты]    Script Date: 09/21/2015 14:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Факультеты](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
	[id_Университета] [int] NULL,
 CONSTRAINT [PK_Факультеты] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Факультеты] ON
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (2, N'Мехмат                                                                                              ', 1)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (3, N'Физфак                                                                                              ', 1)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (4, N'СИЯЛ                                                                                                ', 1)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (5, N'ГНФ                                                                                                 ', 2)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (6, N'СТФ                                                                                                 ', 2)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (7, N'МТФ                                                                                                 ', 2)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (8, N'Лесотехнический                                                                                     ', 3)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (9, N'Матмех                                                                                              ', 4)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (10, N'Почвоведения                                                                                        ', 5)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (11, N'Геологический                                                                                       ', 5)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (12, N'Радиотехники                                                                                        ', 6)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (13, N'Аэрофизики                                                                                          ', 6)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (14, N'Гуманитарный                                                                                        ', 7)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (15, N'Фотоники                                                                                            ', 8)
INSERT [dbo].[Факультеты] ([id], [Название], [id_Университета]) VALUES (16, N'ФИТ                                                                                                 ', 9)
SET IDENTITY_INSERT [dbo].[Факультеты] OFF
/****** Object:  ForeignKey [FK_Университеты_Города]    Script Date: 09/21/2015 14:48:46 ******/
ALTER TABLE [dbo].[Университеты]  WITH CHECK ADD  CONSTRAINT [FK_Университеты_Города] FOREIGN KEY([id_Города])
REFERENCES [dbo].[Города] ([id])
GO
ALTER TABLE [dbo].[Университеты] CHECK CONSTRAINT [FK_Университеты_Города]
GO
/****** Object:  ForeignKey [FK_Факультеты_Университеты]    Script Date: 09/21/2015 14:48:46 ******/
ALTER TABLE [dbo].[Факультеты]  WITH CHECK ADD  CONSTRAINT [FK_Факультеты_Университеты] FOREIGN KEY([id_Университета])
REFERENCES [dbo].[Университеты] ([id])
GO
ALTER TABLE [dbo].[Факультеты] CHECK CONSTRAINT [FK_Факультеты_Университеты]
GO
