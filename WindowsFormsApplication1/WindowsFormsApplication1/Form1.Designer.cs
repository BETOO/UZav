﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.городаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.учебные_заведенияDataSet = new WindowsFormsApplication1.Учебные_заведенияDataSet();
            this.городаTableAdapter = new WindowsFormsApplication1.Учебные_заведенияDataSetTableAdapters.ГородаTableAdapter();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.fKУниверситетыГородаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.университетыTableAdapter = new WindowsFormsApplication1.Учебные_заведенияDataSetTableAdapters.УниверситетыTableAdapter();
            this.fKУниверситетыГородаBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.fKФакультетыУниверситетыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.факультетыTableAdapter = new WindowsFormsApplication1.Учебные_заведенияDataSetTableAdapters.ФакультетыTableAdapter();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.городаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.учебные_заведенияDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKУниверситетыГородаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKУниверситетыГородаBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKФакультетыУниверситетыBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.городаBindingSource;
            this.comboBox1.DisplayMember = "Название";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(23, 49);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(384, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.ValueMember = "Название";
            // 
            // городаBindingSource
            // 
            this.городаBindingSource.DataMember = "Города";
            this.городаBindingSource.DataSource = this.учебные_заведенияDataSet;
            // 
            // учебные_заведенияDataSet
            // 
            this.учебные_заведенияDataSet.DataSetName = "Учебные_заведенияDataSet";
            this.учебные_заведенияDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // городаTableAdapter
            // 
            this.городаTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.fKУниверситетыГородаBindingSource;
            this.comboBox2.DisplayMember = "Название";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(23, 93);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(384, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.ValueMember = "Название";
            // 
            // fKУниверситетыГородаBindingSource
            // 
            this.fKУниверситетыГородаBindingSource.DataMember = "FK_Университеты_Города";
            this.fKУниверситетыГородаBindingSource.DataSource = this.городаBindingSource;
            // 
            // университетыTableAdapter
            // 
            this.университетыTableAdapter.ClearBeforeFill = true;
            // 
            // fKУниверситетыГородаBindingSource1
            // 
            this.fKУниверситетыГородаBindingSource1.DataMember = "FK_Университеты_Города";
            this.fKУниверситетыГородаBindingSource1.DataSource = this.городаBindingSource;
            // 
            // fKФакультетыУниверситетыBindingSource
            // 
            this.fKФакультетыУниверситетыBindingSource.DataMember = "FK_Факультеты_Университеты";
            this.fKФакультетыУниверситетыBindingSource.DataSource = this.fKУниверситетыГородаBindingSource;
            // 
            // факультетыTableAdapter
            // 
            this.факультетыTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox3
            // 
            this.comboBox3.DataSource = this.fKФакультетыУниверситетыBindingSource;
            this.comboBox3.DisplayMember = "Название";
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(23, 135);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(384, 21);
            this.comboBox3.TabIndex = 2;
            this.comboBox3.ValueMember = "Название";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 299);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.городаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.учебные_заведенияDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKУниверситетыГородаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKУниверситетыГородаBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKФакультетыУниверситетыBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private Учебные_заведенияDataSet учебные_заведенияDataSet;
        private System.Windows.Forms.BindingSource городаBindingSource;
        private Учебные_заведенияDataSetTableAdapters.ГородаTableAdapter городаTableAdapter;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.BindingSource fKУниверситетыГородаBindingSource;
        private Учебные_заведенияDataSetTableAdapters.УниверситетыTableAdapter университетыTableAdapter;
        private System.Windows.Forms.BindingSource fKУниверситетыГородаBindingSource1;
        private System.Windows.Forms.BindingSource fKФакультетыУниверситетыBindingSource;
        private Учебные_заведенияDataSetTableAdapters.ФакультетыTableAdapter факультетыTableAdapter;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}

