﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "учебные_заведенияDataSet.Факультеты". При необходимости она может быть перемещена или удалена.
            this.факультетыTableAdapter.Fill(this.учебные_заведенияDataSet.Факультеты);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "учебные_заведенияDataSet.Университеты". При необходимости она может быть перемещена или удалена.
            this.университетыTableAdapter.Fill(this.учебные_заведенияDataSet.Университеты);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "учебные_заведенияDataSet.Города". При необходимости она может быть перемещена или удалена.
            this.городаTableAdapter.Fill(this.учебные_заведенияDataSet.Города);

        }
    }
}
